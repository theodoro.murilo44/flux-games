using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData
{
    public int Level;
    public int Health;
    public int ShieldHealth;
    public bool ActiveShield;
    public int Score;
    public int EnemiesDead;
    public int ShootingSelected;

    public SaveData(int level, bool activeShield, int score, int enemiesDead, int shootingSelected)
    {
        Level = level;
        ActiveShield = activeShield;
        Score = score;
        EnemiesDead = enemiesDead;
        ShootingSelected = shootingSelected;
    }
}
