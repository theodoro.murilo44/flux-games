﻿using Gaminho;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlGame : MonoBehaviour {
    public ScenarioLimits ScenarioLimit;
    public AudioListener audioListener;
    public Text pauseText;
    public Level[] Levels;
    public Image Background;
    [Header("UI")]
    public Text TextStart;
    public Text TextPoints;
    public Transform BarLife;

    private static bool isPaused;

    public static bool IsPaused { get => isPaused; }

    // Use this for initialization
    void Start () {
        Statics.EnemiesDead = 0;
        Background.sprite = Levels[Statics.CurrentLevel].Background;
        TextStart.text = "Stage " + (Statics.CurrentLevel + 1);
        GetComponent<AudioSource>().PlayOneShot(Levels[Statics.CurrentLevel].AudioLvl);
        
    }

    private void Update()
    {
        TextPoints.text = Statics.Points.ToString();
        BarLife.localScale = new Vector3(Statics.Life / 10f, 1, 1);

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (ControlGame.isPaused)
                Resume();
            else
                Pause();
        }
    }

    public void LevelPassed()
    {
        Clear();
        Statics.CurrentLevel++;
        Statics.Points += 1000 * Statics.CurrentLevel;
        if (Statics.CurrentLevel < 3)
        {
            SaveSystem.SaveGame(Statics.CurrentLevel, Statics.WithShield, Statics.Points, Statics.EnemiesDead, Statics.ShootingSelected);
            GameObject.Instantiate(Resources.Load(Statics.PREFAB_LEVELUP) as GameObject);
        }
        else
        {
            SaveSystem.SaveGame(0, false, 0, 0, 2);
            GameObject.Instantiate(Resources.Load(Statics.PREFAB_CONGRATULATION) as GameObject);
        }
    }

    //Oops, when you lose (: Starts from Zero
    public void GameOver()
    {
        BarLife.localScale = new Vector3(0, 1, 1);
        Clear();
        Destroy(Statics.Player.gameObject);
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_GAMEOVER) as GameObject);
    }
    private void Clear()
    {
        GetComponent<AudioSource>().Stop();
        GameObject[] Enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject ini in Enemies)
        {
            Destroy(ini);
        }
    }

    private void Pause()
    {
        Time.timeScale = 0f;
        AudioListener.pause = true;
        pauseText.gameObject.SetActive(true);
        ControlGame.isPaused = true;
    }

    void Resume()
    {
        Time.timeScale = 1f;
        AudioListener.pause = false;
        pauseText.gameObject.SetActive(false);
        ControlGame.isPaused = false;
    }
}
