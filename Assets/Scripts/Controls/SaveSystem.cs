using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    public static void SaveGame(int level, bool activeShield, int score, int enemiesDead, int shootingSelected)
    {
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/save.sav";

        FileStream stream = new FileStream(path, FileMode.Create);
        SaveData saveData = new SaveData(level, activeShield, score, enemiesDead, shootingSelected);

        binaryFormatter.Serialize(stream, saveData);

        stream.Close();
    }

    public static SaveData LoadGame()
    {
        string path = Application.persistentDataPath + "/save.sav";
        if(File.Exists(path))
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);


            SaveData data = (SaveData) binaryFormatter.Deserialize(stream);
            stream.Close();
            return data;
        }
        else
        {
            Debug.LogError("save file not found");
            return null;
        }
    }
}
